package java8scalacheck

import java.time._
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary._

object Java8Arbitrary {

  private def arb[A: Arbitrary] = implicitly[Arbitrary[A]]

  private def mapArb[A, B](arb: Arbitrary[A])(f: A => B): Arbitrary[B] =
    Arbitrary(arb.arbitrary.map(f))

  private def fromInt[A](f: Int => A): Arbitrary[A] =
    Arbitrary(arb[Int].arbitrary.map(f))

  private val smallIntArb = Arbitrary(Gen.choose(1, 100000))

  implicit val DurationArbitrary: Arbitrary[Duration] =
    Arbitrary(Gen.oneOf(
      Gen.resultOf(Duration.ofSeconds(_: Int, _: Int)),
      Gen.resultOf(Duration.ofDays(_: Int)),
      Gen.resultOf(Duration.ofHours(_: Int)),
      Gen.resultOf(Duration.ofMillis(_: Int)),
      Gen.resultOf(Duration.ofMinutes(_: Int)),
      Gen.resultOf(Duration.ofNanos(_: Int))
    ))

  implicit val PeriodArbitrary: Arbitrary[Period] =
    fromInt(Period.ofDays)

  implicit val LocalDateTimeArbitrary: Arbitrary[LocalDateTime] =
    Arbitrary(Gen.resultOf(
      LocalDateTime.of(_: Int, _: Int, _: Int, _: Int, _: Int, _: Int, _: Int)
    ))

  implicit val LocalDateArbitrary: Arbitrary[LocalDate] =
    fromInt(LocalDate.ofEpochDay(_))

  implicit val LocalTimeArbitrary: Arbitrary[LocalTime] =
    Arbitrary(Gen.resultOf(LocalTime.of(_: Int, _: Int, _: Int, _: Int)))

  implicit val InstantArbitrary: Arbitrary[Instant] =
    Arbitrary(Gen.oneOf(
      Gen.resultOf(Instant.ofEpochMilli(_: Int)),
      Gen.resultOf(Instant.ofEpochSecond(_: Int))
    ))

  implicit val YearMonthArbitrary: Arbitrary[YearMonth] =
    Arbitrary(Gen.resultOf(YearMonth.of(_: Int, _: Int)))

  implicit val MonthDayArbitrary: Arbitrary[MonthDay] =
    Arbitrary(Gen.resultOf(MonthDay.of(_: Int, _: Int)))
}
