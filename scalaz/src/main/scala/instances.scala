package scalaz.java8

import java.time._
import scalaz._

object Instances {
  private[this] trait OrderFromComparable[A <: java.lang.Comparable[A]] extends Order[A] {
    override final def order(x: A, y: A): scalaz.Ordering =
      scalaz.Ordering.fromInt(x compareTo y)
  }

  private[this] def orderFromComparable[A <: java.lang.Comparable[A]]: Order[A] =
    new Order[A] {
      override def order(x: A, y: A) = scalaz.Ordering.fromInt(x compareTo y)
    }

  implicit val durationInstance: Monoid[Duration] with Order[Duration] =
    new Monoid[Duration] with OrderFromComparable[Duration] {
      override val zero = Duration.ZERO
      override def append(f1: Duration, f2: => Duration) = f1.plus(f2)
    }

  implicit val periodInstance: Monoid[Period] with Equal[Period] =
    new Monoid[Period] with Equal[Period] {
      override val zero = Period.ZERO
      override def append(f1: Period, f2: => Period) = f1 plus f2
      override def equal(a1: Period, a2: Period) = a1 == a2
      override def equalIsNatural = true
    }

  implicit val yearInstance: Semigroup[Year] with Order[Year] =
    new Semigroup[Year] with OrderFromComparable[Year] {
      def append(x: Year, y: => Year) = x.plusYears(y.getValue)
    }

  implicit val yearMonthInstance: Order[YearMonth] =
    orderFromComparable[YearMonth]

  implicit val monthDayInstance: Order[MonthDay] =
    orderFromComparable[MonthDay]

  implicit val instantInstance: Order[Instant] =
    orderFromComparable[Instant]

  implicit val localTimeInstance: Order[LocalTime] =
    orderFromComparable[LocalTime]

  implicit val offsetDateTimeInstance: Enum[OffsetDateTime] =
    new Enum[OffsetDateTime] with OrderFromComparable[OffsetDateTime] {
      override def pred(a: OffsetDateTime) = a.minusDays(1)
      override def succ(a: OffsetDateTime) = a.plusDays(1)
    }

  implicit val localDateInstance: Enum[LocalDate] =
    new Enum[LocalDate] with Order[LocalDate] {
      override def order(x: LocalDate, y: LocalDate) = scalaz.Ordering.fromInt(x compareTo y)
      override def pred(a: LocalDate) = a.minusDays(1)
      override def succ(a: LocalDate) = a.plusDays(1)
    }

  implicit val localDateTimeInstance: Enum[LocalDateTime] =
    new Enum[LocalDateTime] with Order[LocalDateTime] {
      override def order(x: LocalDateTime, y: LocalDateTime) = scalaz.Ordering.fromInt(x compareTo y)
      override def pred(a: LocalDateTime) = a.minusDays(1)
      override def succ(a: LocalDateTime) = a.plusDays(1)
    }
}
