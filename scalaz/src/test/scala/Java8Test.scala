package scalaz

import java.time._
import java8scalacheck.Java8Arbitrary._
import org.specs2.scalaz.Spec
import scalaz.scalacheck.ScalazProperties._
import scalaz.java8.Instances._

class Java8Test extends Spec {

  checkAll("Duration", monoid.laws[Duration])
  checkAll("Duration", order.laws[Duration])

  checkAll("Instant", order.laws[Instant])

  checkAll("LocalDate", enum.laws[LocalDate])

  /*
  checkAll("LocalDateTime", enum.laws[LocalDateTime])

  checkAll("LocalTime", order.laws[LocalTime])

  checkAll("MonthDay", order.laws[MonthDay])

  checkAll("Period", monoid.laws[Period])
  checkAll("Period", equal.laws[Period])

  checkAll("YearMonth", order.laws[YearMonth])

*/
}
